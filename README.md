### ckdt

[scipy](https://github.com/scipy/scipy/tree/main/scipy/spatial/ckdtree/src) ckdtrees as a standalone c++ library.

### Usage

See [example.cpp](./example/main.cpp) for a simple example or here:

```cpp
#include <CKDT.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
    ckdt::v3 points[3] = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
    ckdt::kdtree tree(&points->data[0], 3);

    ckdt::v3 query = {1, 1, 1};
    auto result = tree.knn(&query, 1, 2); 

    // we query for two points, because the first point in the result set is the query point itself
    std::cout << "Nearest neighbor for " << query << ": " << points[result.indices[1]] << " with a distance of " << result.distances[1] << std::endl;
    return 0;
}
```

## Performance comparison to nanoflann

Test setup:

- Pointcloud: "./example/birmingham_block_12 - origin.ply" ~396,000 points
- Release build, -O2, g++ 13.2.1 20230801
- for nanoflann a simple kd-tree with floats was used, [implementation](https://gitlab.com/k.alexander/ma/-/blob/master/library/src/pointcloud/pointcloud.hpp?ref_type=heads#L23)
- [Test code](https://gitlab.com/k.alexander/ma/-/blob/master/library/tests/test_performance.cpp?ref_type=heads)
- tested with [nanobench](https://github.com/martinus/nanobench) v.4.3.11 on Linux 6.8.7-zen1-1-zen with a Ryzen 7 3700X

### Index building tests
<details>
<summary>Tests</summary>

#### Test 1
<details>
  <summary>Config</summary>
  
- nanoflann
    - max leaf size = 10
- ckdt
    - max leaf size = 10
    - balanced tree = false
    - compact nodes = false
    - copy data = false
</details>
<details>
  <summary>Results</summary>
  
|               ns/op |                op/s |    err% |          ins/op |          cyc/op |    IPC |         bra/op |   miss% |     total | benchmark
|--------------------:|--------------------:|--------:|----------------:|----------------:|-------:|---------------:|--------:|----------:|:----------
|       94,013,350.00 |               10.64 |    0.2% |  503,820,063.00 |  395,298,864.00 |  1.275 |  86,628,922.00 |    7.1% |      1.04 | `Nanoflann index building`
|       43,251,445.00 |               23.12 |    0.4% |  148,844,895.00 |  153,765,111.00 |  0.968 |  25,160,289.00 |    9.8% |      0.47 | `CKDT index building`

</details>

#### Test 2
<details>
  <summary>Config</summary>
  
- nanoflann
    - max leaf size = 10
    - `nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex`
    - threads = 2
- ckdt
    - max leaf size = 10
    - balanced tree = false
    - compact nodes = false
    - copy data = false
</details>
<details>
  <summary>Results</summary>
  
|               ns/op |                op/s |    err% |          ins/op |          cyc/op |    IPC |         bra/op |   miss% |     total | benchmark
|--------------------:|--------------------:|--------:|----------------:|----------------:|-------:|---------------:|--------:|----------:|:----------
|       32,491,322.40 |               30.78 |    1.9% |   27,800,245.80 |   11,763,457.50 |  2.363 |   2,839,290.40 |    2.3% |      1.91 | `Nanoflann index building`
|       43,848,060.80 |               22.81 |    1.0% |  148,999,000.60 |  154,644,079.60 |  0.963 |  25,218,071.80 |   10.0% |      2.59 | `CKDT index building`

</details>

#### Test 3
<details>
  <summary>Config</summary>
  
- nanoflann
    - max leaf size = 10
- ckdt
    - max leaf size = 10
    - balanced tree = true
    - compact nodes = false
    - copy data = false
</details>
<details>
  <summary>Results</summary>
  
|               ns/op |                op/s |    err% |          ins/op |          cyc/op |    IPC |         bra/op |   miss% |     total | benchmark
|--------------------:|--------------------:|--------:|----------------:|----------------:|-------:|---------------:|--------:|----------:|:----------
|       92,439,812.00 |               10.82 |    0.5% |  503,819,989.00 |  393,524,643.00 |  1.280 |  86,628,862.00 |    7.1% |      1.02 | `Nanoflann index building`
|       79,758,020.00 |               12.54 |    0.8% |  263,630,042.00 |  317,192,225.00 |  0.831 |  38,742,596.00 |   15.2% |      0.88 | `CKDT index building`


</details>

#### Test 4
<details>
  <summary>Config</summary>
  
- nanoflann
    - max leaf size = 10
- ckdt
    - max leaf size = 10
    - balanced tree = false
    - compact nodes = true
    - copy data = false

</details>
<details>
  <summary>Results</summary>
  
|               ns/op |                op/s |    err% |          ins/op |          cyc/op |    IPC |         bra/op |   miss% |     total | benchmark
|--------------------:|--------------------:|--------:|----------------:|----------------:|-------:|---------------:|--------:|----------:|:----------
|       92,689,073.00 |               10.79 |    0.4% |  503,819,990.00 |  393,789,915.00 |  1.279 |  86,628,863.00 |    7.1% |      1.02 | `Nanoflann index building`
|       51,886,550.00 |               19.27 |    0.4% |  366,730,903.00 |  195,047,625.00 |  1.880 |  55,235,423.00 |    4.5% |      0.57 | `CKDT index building`


</details>
</details>

#### Summary for index building tests

**nanoflann seems to be faster by about 35%** when using `nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex` and two threads for building the index. Using more did not seem to increase the result all that much.


### KNN querying tests, k = 1, num neighbors = 6

<details>
<summary>Tests</summary>

#### Test 1
<details>
  <summary>Config</summary>
  
- nanoflann
    - max leaf size = 10
    - nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex
    - threads = 2
- ckdt
    - max leaf size = 10
    - balanced tree = false
    - compact nodes = false
    - copy data = false
</details>
<details>
  <summary>Results</summary>
  
|               ns/op |                op/s |    err% |          ins/op |          cyc/op |    IPC |         bra/op |   miss% |     total | benchmark
|--------------------:|--------------------:|--------:|----------------:|----------------:|-------:|---------------:|--------:|----------:|:----------
|              935.90 |        **1,068,486.58** |    4.5% |        3,496.35 |        4,021.81 |  0.869 |         514.51 |    8.6% |      0.01 | `Nanoflann knn search, k = 1`
|            2,034.04 |          **491,631.43** |    3.1% |       10,174.36 |        8,646.66 |  1.177 |       1,660.30 |    4.6% |      0.01 | `CKDT knn search, k = 1`

</details>

#### Test 2
<details>
  <summary>Config</summary>
  
- nanoflann
    - max leaf size = 10
    - nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex
    - threads = 2
- ckdt
    - max leaf size = 8
    - balanced tree = false
    - compact nodes = true
    - copy data = false
</details>
<details>
  <summary>Results</summary>
  
|               ns/op |                op/s |    err% |          ins/op |          cyc/op |    IPC |         bra/op |   miss% |     total | benchmark
|--------------------:|--------------------:|--------:|----------------:|----------------:|-------:|---------------:|--------:|----------:|:----------
|              887.38 |        **1,126,916.48** |    0.4% |        3,485.05 |        3,773.49 |  0.924 |         513.07 |    8.2% |      9.65 | `Nanoflann knn search, k = 1`
|            1,767.40 |          **565,802.68** |    1.5% |        9,133.66 |        7,556.20 |  1.209 |       1,504.22 |    4.1% |     19.10 | `CKDT knn search, k = 1`

</details>
</details>


### KNN querying tests, k = 1, num neighbors = 40,000

<details>
  <summary>Tests</summary>

#### Test 1
<details>
  <summary>Config</summary>
  
- nanoflann
    - max leaf size = 10
    - nanoflann::KDTreeSingleIndexAdaptorFlags::SkipInitialBuildIndex
    - threads = 2
- ckdt
    - max leaf size = 10
    - balanced tree = true
    - compact nodes = true
    - copy data = false
</details>
<details>
  <summary>Results</summary>
  
|               ns/op |                op/s |    err% |          ins/op |          cyc/op |    IPC |         bra/op |   miss% |     total | benchmark
|--------------------:|--------------------:|--------:|----------------:|----------------:|-------:|---------------:|--------:|----------:|:----------
|      247,349,755.58 |                **4.04** |    2.3% |5,383,851,457.95 |1,034,436,884.21 |  5.205 |1,242,192,987.10|    0.0% |    329.42 | `Nanoflann knn search, k = 40000`
|        5,249,106.81 |              **190.51** |    0.4% |   28,315,557.58 |   22,612,225.07 |  1.252 |   4,493,755.18 |    8.0% |      6.86 | `CKDT knn search, k = 40000`

</details>
</details>

#### Summary for knn querying

nanoflann seems to be about **twice as fast** as sci-py trees when querying a single point for a few (i.e. six) neighboring points. However, when querying for large amounts of neighbors (i.e. 40,000), something which has to be done very often in RandLA-Net, **sci-py trees are almost 50 times** faster.

## nanoflann vs. ckdt
This isn't an extensive comparsion and is only based on a select few tests.

#### nanoflann
advantages:
- ~35% faster index building compared to ckdt
- ~50% faster knn search for k = 1 and n(neigbors) = 6 compared to ckdt
- offers functions for saving and loading
- template interface, i.e. works for most data type and structure
- mutli-threaded index building

disadvantages:
- knn search for 40,000 neigbors is almost 50 times slower than ckdt
- no threaded querying

#### ckdt
advantages:
- almost 50x faster for large quries compared to nanoflann
- additional index building options (i.e. balanced tree, compact nodes)
- threaded querying (only in sci-py, i.e. isn't implemented in this c++ version)

disadvantages:
- slower index building compared to nanoflann
- slower knn search for low neighbor count
- rigid interface, i.e. only support for double
- no multi-threaded index building

For RandLA-Net sorted results and fast knn search for large neighborhoods is important, which seems to make ckdt a better library for it.