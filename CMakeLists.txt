cmake_minimum_required(VERSION 3.10)
project(ckdt)
set(CMAKE_CXX_STANDARD 20)

include(cmake/checks.cmake)

add_subdirectory(src)
add_executable(example example/main.cpp)
target_compile_definitions(example PRIVATE -DTEST_DATA="${CMAKE_CURRENT_SOURCE_DIR}/example")

target_link_libraries(example PRIVATE ckdt)