#include <CKDT.hpp>
#include <iostream>

#include "happly.h"
#include "nanoflann.hpp"

inline void save_ply(std::vector<ckdt::v3>& points, std::string file)
{
    // save the result to a new PLY file
    happly::PLYData plyOut;

    // Add mesh data (elements are created automatically)
    std::string const vertexName = "vertex";
    size_t const N = points.size();

    plyOut.addElement(vertexName, N);
    auto& vertexElement = plyOut.getElement(vertexName);

    // De-interleave
    std::vector<float> xPos(N);
    std::vector<float> yPos(N);
    std::vector<float> zPos(N);
    for (size_t i = 0; i < points.size(); i++) {
        xPos[i] = points[i].x;
        yPos[i] = points[i].y;
        zPos[i] = points[i].z;
    }
    vertexElement.addProperty<float>("x", xPos);
    vertexElement.addProperty<float>("y", yPos);
    vertexElement.addProperty<float>("z", zPos);

    plyOut.write(TEST_DATA "/" + file);
}

struct pointcloud {
    std::vector<std::array<double, 3>>* points;

    pointcloud(auto _points)
        : points(_points)
    {
    }

    inline size_t kdtree_get_point_count() const { return points->size(); }

    inline float kdtree_get_pt(const size_t idx, int dim) const
    {
        return (*points)[idx][dim];
    }

       template<class BBOX>
    bool kdtree_get_bbox(BBOX&) const { return false; }
};

typedef nanoflann::KDTreeSingleIndexAdaptor<nanoflann::L2_Simple_Adaptor<double, pointcloud>,
    pointcloud,
    3 /* dim */>
    nfkdtree;


int main(int argc, char* argv[])
{
    happly::PLYData plyIn(TEST_DATA "/birmingham_block_12_cutout.ply");

    if (!plyIn.hasElement("vertex")) {
        return -1;
    }
    std::vector<std::array<double, 3>> in_pos = plyIn.getVertexPositions();
    std::vector<ckdt::v3> points;
    points.reserve(in_pos.size());

    for (auto& pos : in_pos) {
        points.push_back({ pos[0], pos[1], pos[2] });
    }

    ckdt::kdtree tree(points[0].data, in_pos.size(), 3);

    ckdt::v3 query = { -26, -15, -2 };
    auto result = tree.knn(&query, 1, 10000);

    std::vector<ckdt::v3> result_points_all;
    std::vector<ckdt::v3> result_points_5k;
    int i = 0;

    for (auto& idx : result.indices) {
        result_points_all.push_back(points[idx]);
        if (i < 5000) {
            result_points_5k.push_back(points[idx]);
        }
        i++;
    }

    save_ply(result_points_all, "10k.ply");
    save_ply(result_points_5k, "5k.ply");

    pointcloud pc(&in_pos);
    
    nfkdtree nftree(3, pc);
    nftree.buildIndex();

    std::vector<uint32_t> result_indices(10000);
    std::vector<double> result_dists(10000);
    nftree.knnSearch(query.data, 10000, result_indices.data(), result_dists.data());
    result_points_5k.clear(); 
    result_points_all.clear();
    for (int i = 0; i < 10000; i++) {
        if (i <  000)
            result_points_5k.push_back(points[result_indices[i]]);
        result_points_all.push_back(points[result_indices[i]]);
    }
    save_ply(result_points_5k, "5k_nanoflann.ply");
    save_ply(result_points_all, "10k_nanoflann.ply");
    return 0;
}
