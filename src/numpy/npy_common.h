#pragma once

#ifdef HAVE___BUILTIN_EXPECT
#define NPY_LIKELY(x) __builtin_expect(!!(x), 1)
#define NPY_UNLIKELY(x) __builtin_expect(!!(x), 0)
#else
#define NPY_LIKELY(x) (x)
#define NPY_UNLIKELY(x) (x)
#endif

#ifdef HAVE___BUILTIN_PREFETCH
#include <xmmintrin.h>
/* unlike _mm_prefetch also works on non-x86 */
#define NPY_PREFETCH(x, rw, loc) __builtin_prefetch((x), _MM_HINT_NTA) // is this correct? Who knows...
#else
#ifdef NPY_HAVE_SSE
#include <xmmintrin.h>
/* _MM_HINT_ET[01] (rw = 1) unsupported, only available in gcc >= 4.9 */
// so the numpy macro has the `loc` parameter, which is used here with a chain of ternary operators to determine the prefetch hint
// but that causes compilation errors, since the code base only uses loc = 0, we can just hardcode __MM_HINT_NTA
#if defined(_MSC_VER)
#define NPY_PREFETCH(x, rw, loc) _mm_prefetch((const char*) (x), _MM_HINT_NTA)
#else
#define NPY_PREFETCH(x, rw, loc) _mm_prefetch((x), _MM_HINT_NTA)
#endif
#else
#define NPY_PREFETCH(x, rw, loc)
#endif
#endif
#include <sys/types.h>
#include <cstddef>
#include <cstdint>

#if defined(_MSC_VER)
#    include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

typedef ssize_t Py_ssize_t;
typedef Py_ssize_t npy_intp;